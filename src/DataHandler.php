<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

abstract class DataHandler
{
    protected $dispatcher;//EventDispatcherInterface
    protected $eventClass;//string

    public function __construct(EventDispatcherInterface $dispatcher, string $eventClass)
    {
        $this->dispatcher = $dispatcher;

        $this->eventClass = $eventClass;
    }

    public function getOneBy(array $params): DataObjectInterface
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('findOneBy')
                ->setParams($params)
        )->getObjectResult();
    }

    public function queryOneBy(array $params): DataObjectInterface
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('queryOneBy')
                ->setParams($params)
        )->getObjectResult();
    }

    public function getAll(): array
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('findAll')
        )->getArrayResult();
    }

    public function getAllPaginated(array $params): array
    {
        $dataEvent = new $this->eventClass();

        $dataEvent->setMethod('findBy')
            ->setParams($params);

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('findBy')
                ->setParams($params)
        )->getArrayResult();
    }

    public function queryAllPaginated(array $params): array
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('queryBy')
                ->setParams($params)
        )->getArrayResult();
    }

    public function getRowsNumber(): int
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('getRowsNumber')
        )->getIntResult();
    }

    public function getByEnhanced(array $params): array
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('findByEnhanced')
                ->setParams($params)
                ->setIdType($params['id_type'])
                ->{'set' . ucfirst($params['id_type']) . 'Id'}($params['id'])
        )->getArrayResult();
    }

    public function create(array $params): string
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('create')
                ->setParams($params)
        )->getStrResult();
    }

    public function createUnique(array $params): ?string
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('createUnique')
                ->setParams($params)
        )->getStrResult();
    }

    public function update(array $params): int
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('update')
                ->setParams($params)
        )->getIntResult();
    }

    public function updateUnique(array $params): ?int
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('updateUnique')
                ->setParams($params)
        )->getIntResult();
    }

    public function delete(array $params): int
    {
        $dataEvent = new $this->eventClass();

        return $this->dispatcher->dispatch(
            $dataEvent->setMethod('delete')
                ->setParams($params)
        )->getIntResult();
    }
}
