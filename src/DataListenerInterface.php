<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

interface DataListenerInterface
{
    public function execute(DataEventInterface $event): void;
}
