<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

abstract class DataListenerService implements DataListenerInterface
{
    protected $repository;//RepositoryInterface
    protected $query;//QueryInterface
    protected $daoClass;

     public function execute(DataEventInterface $event): void
    {
        $this->{$event->getMethod()}($event);
    }

    protected function findOneBy(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setObjectResult(
            $this->repository->findOneBy(
                $params['criteria']
            )
        );
    }
    protected function queryOneBy(DataEventInterface $event): void
    {
        $params = $event->getParams();

        $event->setObjectResult(
            $this->query->queryOneBy(
                $params['criteria']
            )
        );
    }

    protected function findAll(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $event->setArrayResult(
            $this->repository->findAll()
        );
    }
    protected function findBy(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setArrayResult(
            $this->repository->findBy(
                $params['criteria'],
                $params['order_by'],
                $params['limit'],
                $params['offset']
            )
        );
    }
    protected function queryBy(DataEventInterface $event): void
    {
        $params = $event->getParams();

        $event->setArrayResult(
            $this->query->queryBy(
                $params['criteria'],
                $params['order_by'],
                $params['limit'],
                $params['offset']
            )
        );
    }
    protected function findByEnhanced(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setArrayResult(
            $this->repository->findByEnhanced(
                $params['criteria'],
                $params['operator'],
                $event->{'get' . $event->getIdType() . 'Id'}()
            )
        );
    }

    protected function create(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $dao = new $this->daoClass();
        $dao->hydrate($params['attributes']);

        $event->setStrResult(
            $this->repository->create($dao)
        );
    }
    protected function update(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $dao = new $this->daoClass();
        $dao->hydrate($params['attributes']);

        $event->setIntResult(
            $this->repository->update(
                $params['criteria'],
                $dao
            )
        );
    }
    protected function delete(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->delete(
                $params['criteria'],
            )
        );
    }

    protected function createUnique(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $dao = new $this->daoClass();
        $dao->hydrate($params['attributes']);

        $event->setStrResult(
            $this->repository->createUnique(
                $params['criteria'],
                $dao
            )
        );
    }
    protected function updateUnique(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $dao = new $this->daoClass();
        $dao->hydrate($params['attributes']);

        $event->setIntResult(
            $this->repository->updateUnique(
                $params['criteriaS'],
                $params['criteriaU'],
                $dao
            )
        );
    }

    protected function createMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->createMultiple(
                $params['fields_sets']
            )
        );
    }
    protected function updateMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->updateMultiple(
                $params['fields_sets']
            )
        );
    }
    protected function deleteMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->deleteMultiple(
                $params['ids']
            )
        );
    }

    protected function getRowsNumber(DataEventInterface $event): void
    {
        $event->setIntResult(
            $this->repository->getRowsNumber()
        );
    }
}
