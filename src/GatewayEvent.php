<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

class GatewayEvent implements GatewayEventInterface
{
    private $action;
    private $params;
    private $jsonResult;

    public function getAction(): ?string
    {
        return $this->action;
    }
    public function setAction(string $action): GatewayEventInterface
    {
        $this->action = $action;
        return $this;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }
    public function setParams(array $params): GatewayEventInterface
    {
        $this->params = $params;
        return $this;
    }

    public function getJsonResult(): ?string
    {
        return $this->jsonResult;
    }
    public function setJsonResult(?string $jsonResult): GatewayEventInterface
    {
        $this->jsonResult = $jsonResult;
        return $this;
    }
}
