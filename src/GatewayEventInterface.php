<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

interface GatewayEventInterface
{
    public function getAction(): ?string;
    public function setAction(string $action): GatewayEventInterface;

    public function getParams(): ?array;
    public function setParams(array $params): GatewayEventInterface;

    public function getJsonResult(): ?string;
    public function setJsonResult(?string $jsonResult): GatewayEventInterface;
}
