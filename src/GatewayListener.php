<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

use Psr\Container\ContainerInterface;

class GatewayListener implements GatewayListenerInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function execute(GatewayEventInterface $event): void
    {
        $action = $this->container->get($event->getAction());

        $event->setJsonResult($action($event->getParams()));
    }
}
