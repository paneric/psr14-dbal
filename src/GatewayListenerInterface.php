<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

interface GatewayListenerInterface
{
    public function execute(GatewayEventInterface $event): void;
}
