<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

interface QueryInterface
{
    public function adaptManager(): void;
}
