<?php

declare(strict_types=1);

namespace Paneric\Psr14DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface RepositoryInterface
{
    public function findOneBy(array $criteria): ?DataObjectInterface;

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string;
    public function updateUnique(array $criteriaSelect, array $criteriaUpdate, DataObjectInterface $dataObject): ?int;
    public function delete(array $criteria): int;

    public function getRowsNumber(): int;
}
